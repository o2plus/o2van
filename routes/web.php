<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ViewController@orderTable');

Auth::routes();

//test
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/sally', 'ViewController@sally');

//page
Route::get('/order', 'ViewController@orderRegister');
Route::get('/order/table', 'ViewController@orderTable');
Route::get('/customer', 'ViewController@customerRegister');

//Api


Route::prefix('/api')->group(function () {
    Route::get('/order', 'ApiController@getOrders');
    Route::get('/new_order', 'ApiController@newOrder');
    Route::get('/detail_order/{order_id}','ApiController@detailOrder');
});
