/**
 * Created by myeongkyuhwang on 2017. 7. 13..
 */

var o2q = {};
o2q.send = function( url , params , funcSuccess, funcFail ) {
    var doFuncSuccess = funcSuccess || function() {};
    var doFuncFail = funcFail || function() {};
    $.get( url , params , function( data ) {
        if( data.data && data.code == 200 ) {
            doFuncSuccess( data.data );
        } else {
            doFuncSuccess( data );
        }
    })
    .fail(function() {
        doFuncFail();
    })
    ;
}
o2q.sendPost = function( url , oParams , funcSuccess , funcFail ) {
    var doFuncSuccess = funcSuccess || function() {};
    var doFuncFail = funcFail || function() {};
    var willBeUsedParams = {};

    if( oParams ) {
        if( $( oParams ).get(0).tagName == "FORM" ) {
            willBeUsedParams = $( oParams ).serializeArray();
        } else {
            willBeUsedParams = oParams || {};
        }
    } else{
        willBeUsedParams = {};
    }

    $.ajax({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
        method: "POST",
        url: url ,
        data: willBeUsedParams
    })
    .done(function( data ) {
        if( data.data && data.code == 200 ) {
            doFuncSuccess( data.data );
        } else {
            doFuncSuccess( data );
        }

    })
    .fail( function( data ) {
        doFuncFail( data.responseJSON );
    } )
    ;

}

o2q.convertCost = function()
{

}

o2q.order = {};
o2q.order.submit=function( isRealOrder,_onComplete ){

    // 수정 모드 이면 변경이력이 있는지 체크하여 변경 이력이 없으면 submit 하지 않는다 OQ-145
    if( pureFormSerialized ) {
        is_update_real_order= false;
        $(pureFormSerialized).each( function(k,v) {
            if( v.name == "insung_order_no" ) {
                if( v.value.length > 0 ) {
                    is_update_real_order= true;
                }
            }
        });

        if( isRealOrder && !is_update_real_order ) {
            // 실주문 처리 이면서, 이전 주문이 실주문 아닌경우. ( 임시주문 -> 실주문 ) 변경이력 스킵.
        } else{
            console.log( pureFormSerialized );
            isDirty = false;
            thisFormSerialized= $("#order_form").serializeArray();
            $(pureFormSerialized).each( function(k,v) {
                $(thisFormSerialized).each( function(k2,v2) {
                    if( v.name == v2.name && $.inArray( v.name, [ "delivery_address_type" ] ) == -1 ) {
                        if( v.value != v2.value ) {
                            isDirty = true;
                        }
                    }
                });
            });

            if( !isDirty ) {
                var confirm=window.confirm('수정내역이 없습니다. 창을 닫을까요?\n 확인:닫기 취소:남겨둠');
                if(confirm){
                    $("#select",opener.document).trigger("click");
                    window.close();
                }

                return false;
            }
        }

    }

    $("button#tempSubmit").prop("disabled",true).text("...");
    $("button#submit").prop("disabled",true).text("...");
    $("button#submitUpdate").prop("disabled",true).text("...");
    $("button#submitRealUpdate").prop("disabled",true).text("...");
    $("button#submitCancel").prop("disabled",true).text("...");

    var _f = $("#order_form");

    _f.find("input[name=CID]").val( $("#CID").val() );
    _f.find("input[name=is_real_order]").val( isRealOrder );

    _f.find("[name=charge_name]").val( _f.find("[name=receipt_name]").val());
    if( _f.find("[name=receipt_tel_no1]").val() && _f.find("[name=receipt_tel_no2]").val() && _f.find("[name=receipt_tel_no3]").val() ) {
        _f.find("[name=charge_phone1]").val( _f.find("[name=receipt_tel_no1]").val() );
        _f.find("[name=charge_phone2]").val( _f.find("[name=receipt_tel_no2]").val() );
        _f.find("[name=charge_phone3]").val( _f.find("[name=receipt_tel_no3]").val() );
    } else {
        _f.find("[name=charge_phone1]").val( _f.find("[name=receipt_phone1]").val() );
        _f.find("[name=charge_phone2]").val( _f.find("[name=receipt_phone2]").val() );
        _f.find("[name=charge_phone3]").val( _f.find("[name=receipt_phone3]").val() );
    }
    var validate=o2q.order.validate($("#order_form"));
    if(!validate.status){
        alert(validate.msg);
        $("button#submit").prop("disabled",false).text("주문");
        $("button#tempSubmit").prop("disabled",false).text("임시주문");
        $("button#submitUpdate").prop("disabled",false).text("수정");
        $("button#submitRealUpdate").prop("disabled",false).text("수정 실주문");
        $("button#submitCancel").prop("disabled",false).text("취소");

        return false;
    }
    if($("#order_form").find("input[name=start_geo_id]").val()==$("#order_form").find("input[name=dest_geo_id]").val()){
        var geo_same=window.confirm('출발지와 도착지가 같습니다. 계속 하시겠습니까?');
        if(!geo_same){
            $("button#submit").prop("disabled",false).text("주문");
            $("button#tempSubmit").prop("disabled",false).text("임시주문");
            $("button#submitUpdate").prop("disabled",false).text("수정");
            $("button#submitRealUpdate").prop("disabled",false).text("수정 실주문");
            $("button#submitCancel").prop("disabled",false).text("취소");
            return false;
        }
    }
    o2q.sendPost('/v1/order',$("#order_form"),function(data){
        $("button#submit").prop("disabled",false).text("주문");
        if( typeof _onComplete == "function" ) {
            _onComplete( data );
        }
        // window.location.href = '/usage';
    },function(data){
        alert(data.msg);
    });
}

o2q.order.validate=function(orderForm){

    console.log("reservate check..", $("#datetimepicker1").data("date") , moment().format( "YYYY-MM-DD HH:mm" ) );


    var status=true;
    var msg='';
    var _f = $("#order_form");
    if( _f.find("[name=start_geo_id]").val()=='' || _f.find("[name=dest_geo_id]").val()=='' ) {
        status= false;
        msg+= "- 주소정보가 정확하지 않습니다.";
    }

    if( !_f.find("[name=start_tel_no]").val() && !_f.find("[name=start_phone]").val() ) {
        status= false;
        msg+= "- 시작지 연락처는 필수 입니다.";
    }

    if( !_f.find("[name=dest_tel_no]").val() && !_f.find("[name=dest_phone]").val() ) {
        status= false;
        msg+= "- 도착지 연락처는 필수 입니다.";
    }
    if( $("#orderForm").find("[name=receipt_tel_no]").length ) {
        if( !( $(orderForm).find("[name=receipt_tel_no]").val() )
            && !( $(orderForm).find("[name=receipt_phone]").val() )
        ) {
            status= false;
            msg+= "- 접수자의 전화번호 혹은 휴대폰 번호가 존재해야 합니다.";
        }
    }

    if( $(orderForm).find("#order_distance").val()==''){
        status=false;
        msg+='- 위치정보가 정확하지않아 주문할 수 없습니다.\n(거리산정이 필요합니다.)';
    }
    var items=$(orderForm).find(".item_select");
    var item=0;
    $.each(items,function(k,v){
        item+=$(v).val();
    })
    if(item==0){
        status=false;
        msg+='- 배송품목을 선택해주세요.\n';
    }

    return {"status":status,"msg":msg};
}
o2q.order.dpcheck=function(){
    if($("input[name=start_geo_id]").val()!='' &&  $("input[name=dest_geo_id]").val()!=''){
        o2q.order.distancecheck( $("input[name=start_geo_id]").val(), $("input[name=dest_geo_id]").val() );
    }
}
o2q.order.distancecheck=function( s_geo_id, d_geo_id){
    $("button#tempSubmit").prop("disabled",true);
    $("#submit").prop("disabled",true);
    $("td[name=distance]").text( "" );
    o2q.send('/v1/distance/'+s_geo_id+'/'+d_geo_id,null,function(data){
        if( data.data && data.code && data.code != "200" ) {
            alert( data.data );
        } else {
            $("td[name=distance]").text(data.distance+' Km');
        }
        o2q.order.pricecheck();
    },function(data){

    });

}
o2q.order.pricecheck=function(){
    if( $("input[name=start_geo_id]").val()=='' || !$("input[name=start_geo_id]").val() || $("input[name=dest_geo_id]").val()=='' || !$("input[name=dest_geo_id]").val() ){
        return false;
    }
    $("#submit").prop("disabled",true);
    $("button#tempSubmit").prop("disabled",true);
    $("td[name=price]").text( "" );
    o2q.sendPost('/v1/price',$("#order_form"),function(data){
        $("td[name=orderUse]").text(Number(data.orderUse).toLocaleString());
        $("td[name=basic_price]").text(Number(data.basic).toLocaleString());
        $("td[name=add_price]").text(Number(data.add).toLocaleString());
        $("td[name=consign_price]").text(Number(data.consign).toLocaleString());
        $("td[name=discount_price]").text(Number(data.discount).toLocaleString());
        $("td[name=price]").text(Number(data.all).toLocaleString());
        if(Number(data)==0){
            alert('가격이 0원인 주문도 약관에 따라 정상적으로 정산합니다.')
        }
        $("#submit").prop("disabled",false);
        $("button#tempSubmit").prop("disabled",false);
    },function(data){
        alert(data.msg);
        $("#submit").prop("disabled",false);
        $("button#tempSubmit").prop("disabled",false);
    });
}

// 즐겨 찾기 등록 및 수정
function upsertUser( params , member_id )
{
    var popup=true;
    params['member_id']= member_id;
    if(typeof params['start_user_id']!='undefined'){
        if(params['start_user_id']==''){
            params['createOrUpdate']=true;
            popup=false;
        }
    }else if(typeof params['dest_user_id']!='undefined'){
        if(params['dest_user_id']==''){
            params['createOrUpdate']=true;
            popup=false;
        }
    }else if(typeof params['shuttle_user_id']!='undefined'){
        if(params['shuttle_user_id']==''){
            params['createOrUpdate']=true;
            popup=false;
        }
    }
    if(popup){
        params['createOrUpdate']=window.confirm('새로 등록하시겠습니까?(확인:예 취소:아니요)');
    }
    o2q.sendPost('/client/user_edit',params,function(data){
        alert("즐겨찾기가 저장 되었습니다.");
        if( typeof get_user_favorites == "function" ) {
            get_user_favorites( member_id );
        }
    },function(){
        alert("에러. 개발사 문의.");
    } );
}

function getOrderMap(id)
{
    window.open("/order/map/"+id);
}

function image_render(url){
    if(url!=null)return '<img src="'+url+'"/>';
    else return'';
}


$( function() {
    $(document).keydown(function(e){
        
    });
});