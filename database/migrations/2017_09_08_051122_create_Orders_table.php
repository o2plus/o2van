<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	public function up()
	{
		Schema::create('Orders', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('item', 50)->nullable();
			$table->integer('item_amount')->nullable();
			$table->integer('item_kg')->nullable();
			$table->text('etc')->nullable();
			$table->text('note')->nullable();
			$table->text('memo')->nullable();
			$table->boolean('take_reciept')->default(false);
			$table->boolean('tax')->default(false);
			$table->boolean('internet')->default(false);	
			$table->date('bill_date')->nullable();
			$table->date('pay_date')->nullable();
			$table->integer('buyer_id')->unsigned();
			$table->integer('seller_id')->unsigned();
			$table->integer('buy_price')->nullable();
			$table->integer('sell_price')->nullable();
			$table->integer('dest_price')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('Orders');
	}
}