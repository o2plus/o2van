@extends('layouts.dash')
@section('title','주문 조회')

@section('content')
    <div class='col-lg-12'>
        @include('template.search_table')
    </div>
    <div class='row'>
        <div class='col-lg-12'>
            <form id="orderTable_form">
                <div id="table_wrapper" class="dataTables_wrapper no_footer">
                    <div class="dataTables_scroll">
                        <div class="dataTables_scrollHead" style="overflow: hidden; position: relative; width: 100%;">
                            <div class="dataTables_scrollHeadInner" style="box-sizing: content-box;">
                                <table class="display compact dataTable no-footer" role="grid">
                                    <thead>
                                    <tr role="row">
                                        <th>NO</th>
                                        <th>날짜</th>
                                        <th>매출처</th>
                                        <th>발지</th>
                                        <th>착지</th>
                                        <th>매입처</th>
                                        <th>중량</th>
                                        <th>차량번호</th>
                                        <th>매출액</th>
                                        <th>매입액</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                2017-08-30
                                            </td>
                                            <td>
                                                세경산업 (주)
                                            </td>
                                            <td>세경</td>
                                            <td>시흥정왕동</td>
                                            <td>주차장</td>
                                            <td>5</td>
                                            <td>인천 86바 6688</td>
                                            <td>140,000</td>
                                            <td>130,000</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                2017-08-30
                                            </td>
                                            <td>
                                                세경산업 (주)
                                            </td>
                                            <td>세경</td>
                                            <td>시흥정왕동</td>
                                            <td>주차장</td>
                                            <td>5</td>
                                            <td>인천 86바 6688</td>
                                            <td>140,000</td>
                                            <td>130,000</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                2017-08-30
                                            </td>
                                            <td>
                                                세경산업 (주)
                                            </td>
                                            <td>세경</td>
                                            <td>시흥정왕동</td>
                                            <td>주차장</td>
                                            <td>5</td>
                                            <td>인천 86바 6688</td>
                                            <td>140,000</td>
                                            <td>130,000</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                2017-08-30
                                            </td>
                                            <td>
                                                세경산업 (주)
                                            </td>
                                            <td>세경</td>
                                            <td>시흥정왕동</td>
                                            <td>주차장</td>
                                            <td>5</td>
                                            <td>인천 86바 6688</td>
                                            <td>140,000</td>
                                            <td>130,000</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
