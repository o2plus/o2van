<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="O2quick">
    <meta name="author" content="O2Quick">
    <meta name="keyword" content="O2Quick">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>O2VAN</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/jquery-ui-1.9.2.custom.min.css" rel="stylesheet">
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <link href="/css/bootstrap-slider.min.css" rel="stylesheet">
    <!--external css-->
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/style-responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/DataTables/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-datetimepicker.min.css"/>
    <!-- style -->
    <link href="/assets/css/template/register.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<section id="container" >
    @include('common.header')
    @include('common.side')
    <section id="main-content">
        <section class="wrapper site-min-height">
            <h3 style="margin-left:15px;"> @yield('title')</h3>
            <div id="top_menu" style="float:left;">
                @if(url()->current()==url('/customer') )
                    <div class="nav notify-row" id="top_menu" style="margin-left:15px;">
                        <button class='btn btn-primary'>신규</button>
                        <button class='btn btn-primary'>저장</button>
                        <button class='btn btn-primary'>삭제</button>
                        <button class='btn btn-primary'>인쇄</button>
                        <button class='btn btn-primary'>종료</button>
                        <button class='btn btn-primary'>옵션</button>
                        <button class='btn btn-primary'>DM</button>
                    </div>
                @else
                    <div class="nav notify-row" id="top_menu" style="margin-left:15px;">
                        <button class='btn btn-primary'>신규</button>
                        <button class='btn btn-primary'>저장</button>
                        <button class='btn btn-primary'>삭제</button>
                        <button class='btn btn-primary'>수탁증</button>
                    </div>
                @endif
            </div>
        <!-- **********************************************************************************************************************************************************
            TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
            {{--<header class="header black-bg">--}}
                {{--@if(url()->current()==url('/customer') )--}}
                {{--<div class="nav notify-row" id="top_menu" style='margin-left:10px;'>--}}
                    {{--<button class='btn btn-primary'>신규</button>--}}
                    {{--<button class='btn btn-primary'>저장</button>--}}
                    {{--<button class='btn btn-primary'>삭제</button>--}}
                    {{--<button class='btn btn-primary'>인쇄</button>--}}
                    {{--<button class='btn btn-primary'>종료</button>--}}
                    {{--<button class='btn btn-primary'>옵션</button>--}}
                    {{--<button class='btn btn-primary'>DM</button>--}}
                {{--</div>--}}
                {{--@else--}}
                {{--<div class="nav notify-row" id="top_menu" style='margin-left:10px;'>--}}
                    {{--<button class='btn btn-primary'>신규</button>--}}
                    {{--<button class='btn btn-primary'>저장</button>--}}
                    {{--<button class='btn btn-primary'>삭제</button>--}}
                    {{--<button class='btn btn-primary'>수탁증</button>--}}
                {{--</div>--}}
                {{--@endif--}}
                {{--<div class="top-menu">--}}
                    {{--<ul class="nav pull-right top-menu">--}}
                        {{--<li><a style="cursor:pointer;" class="logout" onclick="event.preventDefault();$('#logout-form').submit()">Logout</a></li>--}}
                    {{--</ul>--}}
                    {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                        {{--{{ csrf_field() }}--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</header>--}}
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
    *********************************************************************************************************************************************************** -->
            <div class="row mt">
                <div class="col-lg-12">
                    @yield('main')
                </div>
            </div>
        </section>
    </section>
</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="/assets/js/jquery.js"></script>
<script src="/assets/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="/assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/js/bootstrap-slider.min.js"></script>
<script class="include" type="text/javascript" src="/assets/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="/assets/js/jquery.scrollTo.min.js"></script>
<script src="/assets/js/jquery.nicescroll.js" type="text/javascript"></script>

<!--common script for all pages-->
<script src="/assets/js/o2common.js"></script>
<script src="/assets/js/common-scripts.js"></script>
<script type="text/javascript" src="/assets/js/jquery.serializeObject.min.js"></script>
<script type="text/javascript" src="/DataTables/datatables.min.js"></script>
<script type="text/javascript" src="/DataTables/colResize-master/dataTables.colResize.js"></script>
<!--script for this page-->

<!-- datetime picker -->
<script type="text/javascript" src="/assets/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap-datetimepicker.min.js"></script>

{{--@include('template.deliveryAddressFormScript')--}}
@stack('js')
</body>
</html>
