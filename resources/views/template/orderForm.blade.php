<div class="row mt" >
    <div class="col-lg-12" style='width:1280px; margin:0 auto;'>
        <div class="row">
            <form id='order_form'>
                <div class="col-lg-6 col-md-6" style="display:inline-block;">
                    <div class="col-lg-12">
                        <div class="panel panel-danger" style="margin-bottom:5px;">
                            <div class="panel-heading">
                                주문정보
                            </div>
                            <div class="panel-body" style="padding:0">
                                <table class="delivery" style="width:100%;">
                                    <tbody>
                                        <tr>
                                            <th>
                                                날짜
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>발지</th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                전표번호
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>착지</th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                매출코드
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>매출단가</th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                매출처
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>매입단가</th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                매입코드
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>기타</th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                매입처
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>비고</th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                품목
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>메모</th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                중량
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>착불금</th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                수량
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>계근비</th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                차량번호
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>인수증</th>
                                            <td>
                                                <input type="checkbox">체크 1
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                청구월
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>세금</th>
                                            <td>
                                                <input type="checkbox">체크 2
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                지급월
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>인터넷</th>
                                            <td>
                                                <input type="checkbox">체크 3
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="2">매출</th>
                                            <th colspan="2">매입</th>
                                        </tr>
                                        <tr>
                                            <th>
                                                매출액
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>매입액</th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                금액
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>금액</th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                구분2
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>구분2</th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                수수료
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>보관료</th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                입금일
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>출금일</th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>
                                                금액
                                            </th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                            <th>금액</th>
                                            <td>
                                                <input type="text" class="form-control">
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="col-xs-8" style='padding-right:0px'>
                        <div class="form-group" style='margin-bottom:0px'>
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control" name="reservated_at" value=""/>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                        @push('js')
                            <script type="text/javascript">
                                $(function () {
                                    $('#datetimepicker1').datetimepicker({
                                        format: "YYYY-MM-DD HH:mm",
                                        stepping:'10',
                                        minDate:'{{\Carbon\Carbon::now()->addMinute(30)->toDateTimeString()}}',
                                        maxDate:'{{\Carbon\Carbon::now()->addDay(30)->toDateTimeString()}}'
                                    });
                                    $('#datetimepicker1 [name=reservated_at]').val('');
                                });
                            </script>
                        @endpush
                    </div>
                    <div class="col-lg-12 mt">
                        <div id="table_wrapper" class="dataTables_wrapper no_footer">
                            <div class="dataTables_scroll">
                                <div class="dataTables_scrollHead" style="overflow: hidden; position: relative; width: 100%;">
                                    <div class="dataTables_scrollHeadInner" style="box-sizing: content-box;">
                                        <table class="display compact dataTable no-footer" role="grid">
                                            <thead>
                                                <tr role="row">
                                                    <th>NO</th>
                                                    <th>매출처</th>
                                                    <th>매입처</th>
                                                    <th>품목</th>
                                                    <th>중량</th>
                                                    <th>차량번호</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>매출처</td>
                                                    <td>매입처</td>
                                                    <td>품목</td>
                                                    <td>중량</td>
                                                    <td>차량번호</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>매출처</td>
                                                    <td>매입처</td>
                                                    <td>품목</td>
                                                    <td>중량</td>
                                                    <td>차량번호</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>매출처</td>
                                                    <td>매입처</td>
                                                    <td>품목</td>
                                                    <td>중량</td>
                                                    <td>차량번호</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>매출처</td>
                                                    <td>매입처</td>
                                                    <td>품목</td>
                                                    <td>중량</td>
                                                    <td>차량번호</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>