<div class="row mt" >
    <div class="col-lg-12" style='width:1280px; margin:0 auto;'>
        <div class="row">
            <form id='customer_form'>
                <div class="col-lg-5">
                    <div class="col-xs-8" style='padding-right:0px'>
                        <div class="col-lg-12" style="margin-bottom:2px; padding:0;">
                            <label style="font-weight: bold; border:1px solid gray; padding:5px;">정렬</label>
                            <span style="padding:6px; background-color: white;">
                                <input type="radio" name="order_by" value="">코드순
                                <input type="radio" name="order_by" value="">상호순
                            </span>
                        </div>
                        <div class="col-lg-12" style="margin-bottom:2px; padding:0;">
                            <label style="font-weight: bold; border:1px solid gray; padding:5px;">구분</label>
                            <span style="padding:6px; background-color: white;">
                                <input type="radio" name="type" value="">화주
                                <input type="radio" name="type" value="">차량
                                <input type="radio" name="type" value="">용차
                                <input type="radio" name="type" value="">기타
                                <input type="radio" name="type" value="">전체
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div id="table_wrapper" class="dataTables_wrapper no_footer">
                            <div class="dataTables_scroll">
                                <div class="dataTables_scrollHead" style="overflow: hidden; position: relative; width: 100%;">
                                    <div class="dataTables_scrollHeadInner" style="box-sizing: content-box;">
                                        <table class="display compact dataTable no-footer" style="margin-bottom:10px; overflow-y: scroll;" role="grid">
                                            <thead>
                                            <tr role="row">
                                                <th>NO</th>
                                                <th>코드</th>
                                                <th>상호</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>10002</td>
                                                <td>광진전설</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>10005</td>
                                                <td>목림</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>10006</td>
                                                <td>홍일기업(주)</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>10007</td>
                                                <td>진로이천공장</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>10008</td>
                                                <td>유석철강(주)</td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>10009</td>
                                                <td>대도물산(주)</td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>10002</td>
                                                <td>광진전설</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>10005</td>
                                                <td>목림</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>10006</td>
                                                <td>홍일기업(주)</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>10007</td>
                                                <td>진로이천공장</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>10008</td>
                                                <td>유석철강(주)</td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>10009</td>
                                                <td>대도물산(주)</td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>10002</td>
                                                <td>광진전설</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>10005</td>
                                                <td>목림</td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>10006</td>
                                                <td>홍일기업(주)</td>
                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>10007</td>
                                                <td>진로이천공장</td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>10008</td>
                                                <td>유석철강(주)</td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>10009</td>
                                                <td>대도물산(주)</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-5" style="display:inline-block;">
                    <div class="col-lg-12">
                        <div class="panel panel-danger" style="margin-bottom:5px;">
                            <div class="panel-heading">
                                거래처 등록
                            </div>
                            <div class="panel-body" style="padding:0">
                                <table class="delivery" style="width:100%;">
                                    <tbody>
                                    <tr>
                                        <th>
                                            거래처구분
                                        </th>
                                        <td>
                                            <input type="radio">화주
                                            <input type="radio">차량
                                            <input type="radio">용차
                                            <input type="radio">기타
                                        </td>
                                        <th>우편번호</th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            거래처코드
                                        </th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                        <th>담당자</th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            거래처명
                                        </th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                        <th>취급품목</th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            사업자번호
                                        </th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                        <th>비고</th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            대표자명
                                        </th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                        <th>발지</th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            업종
                                        </th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                        <th>착지</th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            업태
                                        </th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                        <th>약도(동)</th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            전화번호
                                        </th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                        <th>약도(번)</th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            주소 (동)
                                        </th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                        <th>차종</th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            주소 (번)
                                        </th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                        <th>단가표구분</th>
                                        <td>
                                            <input type="text" class="form-control">
                                        </td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                        <div class="col-lg-12">
                                            <div id="table_wrapper" class="dataTables_wrapper no_footer">
                                                <div class="dataTables_scroll">
                                                    <div class="dataTables_scrollHead" style="overflow: hidden; position: relative; width: 100%;">
                                                        <div class="dataTables_scrollHeadInner" style="box-sizing: content-box;">
                                                            <table class="display compact dataTable no-footer" style="margin-bottom:10px; overflow-y: scroll; margin-top: 10px;" role="grid">
                                                                <thead>
                                                                <tr role="row">
                                                                    <th>NO</th>
                                                                    <th>매출액</th>
                                                                    <th>입금액</th>
                                                                    <th>매입액</th>
                                                                    <th>지급액</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td>0</td>
                                                                        <td>0</td>
                                                                        <td>0</td>
                                                                        <td>0</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>2</td>
                                                                        <td>0</td>
                                                                        <td>0</td>
                                                                        <td>0</td>
                                                                        <td>0</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>3</td>
                                                                        <td>0</td>
                                                                        <td>0</td>
                                                                        <td>0</td>
                                                                        <td>0</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>1</td>
                                                                        <td>0</td>
                                                                        <td>0</td>
                                                                        <td>0</td>
                                                                        <td>0</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>2</td>
                                                                        <td>0</td>
                                                                        <td>0</td>
                                                                        <td>0</td>
                                                                        <td>0</td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td>3</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                    <td>0</td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

