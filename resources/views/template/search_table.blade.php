<div class="row">
    <div class="col-lg-12" style="margin-bottom:20px; padding: 2px;">
        <button class='btn btn-primary'>찾기</button>
        <button class='btn btn-primary'>인쇄</button>
        <button class='btn btn-primary'>엑셀로</button>
    </div>
    <form name="tableForm" id="tableForm">
        <div class="col-lg-7" style="border: 1px solid black;">
            <div>
                <div class="col-xs-5" style='padding-right:0px'>
                    <div class="form-group" style='margin-bottom:0px'>
                        <div class='input-group date' id='startDate'>
                            <input type='text' class="form-control" name="" value=""/>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                    @push('js')
                        <script type="text/javascript">
                            $(function () {
                                $('#startDate').datetimepicker({
                                    format: "YYYY-MM-DD HH:mm",
                                    stepping:'10',
                                    minDate:'{{\Carbon\Carbon::now()->addMinute(30)->toDateTimeString()}}',
                                    maxDate:'{{\Carbon\Carbon::now()->addDay(30)->toDateTimeString()}}'
                                });
                                $('#datetimepicker1 [name=reservated_at]').val('');
                            });
                        </script>
                    @endpush
                </div>
                <div class="col-xs-5" style='padding-right:0px'>
                    <div class="form-group" style='margin-bottom:0px'>
                        <div class='input-group date' id='endDate'>
                            <input type='text' class="form-control" name="" value=""/>
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                    @push('js')
                        <script type="text/javascript">
                            $(function () {
                                $('#endDate').datetimepicker({
                                    format: "YYYY-MM-DD HH:mm",
                                    stepping:'10',
                                    minDate:'{{\Carbon\Carbon::now()->addMinute(30)->toDateTimeString()}}',
                                    maxDate:'{{\Carbon\Carbon::now()->addDay(30)->toDateTimeString()}}'
                                });
                                $('#datetimepicker1 [name=reservated_at]').val('');
                            });
                        </script>
                    @endpush
                </div>
            </div>
        </div>
        {{--<div class="col-lg-2 col-md-6 col-xs-12">--}}
            {{--<div class='row'>
                <label class="btn btn-default col-xs-6 con_state">
                    <input type="checkbox" name="status[]" value="R" id="option1" autocomplete="off"> 접수
                </label>
            </div>--}}
        {{--</div>--}}
    </form>
</div>