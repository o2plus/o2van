<!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
<!--sidebar start-->
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <ul class="sidebar-menu" id="nav-accordion">

            {{--<p class="centered"><a href="profile.html"><img src="assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>--}}
            {{--<h5 class="centered">Marcel Newman</h5>--}}

            <li class="mt">
                <a class="" href="/order">
                    <i class="fa fa-dashboard"></i>
                    <span>주문등록</span>
                </a>
            </li>
            <li class="sub-menu">
                <a class="" href="/customer">
                    <i class="fa fa-dashboard"></i>
                    <span>거래처등록</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="/order/table" >
                    <i class="fa fa-desktop"></i>
                    <span>조회</span>
                </a>
            </li>
        </ul>
    </div>
</aside>
<!--sidebar end-->