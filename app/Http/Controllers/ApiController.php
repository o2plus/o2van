<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models as M;

class ApiController extends Controller
{
    //Order 모델 관련
    public function getOrders() //전체 오더 가져오기
    {
        return M\Order::all();
    }

    public function newOrder() //새로운 오더 번호 가져오기
    {
        return (M\Order::latest()->first()->id)+1;
    }

    public function detailOrder($order_id)
    {
        return M\Order::find($order_id);
    }
    

    //User 모델 관련
    public function getUser(Request $request) //전체 오더 가져오기
    {
        $query=M\User::select('*');
        return $query->get();
    }
    
    public function newUser() //새로운 오더 번호 가져오기
    {
        
    }
}
