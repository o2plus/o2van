<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ViewController extends Controller
{
    public function sally()
    {
        return view( "sally" );
    }

    public function orderRegister(){
        return view("page.order");
    }
    public function customerRegister(){
        return view("page.customer");
    }
    public function orderTable(){
        return view("page.order_table");
    }
}
